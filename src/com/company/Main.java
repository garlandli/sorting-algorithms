package com.company;


public class Main {
    public static void main(String[] args) {
        int[] numbers = {1, 2, 5, 3, 0, 4};

        Sort alg = new Sort();
        alg.bubbleSort(numbers);
        alg.selectionSort(numbers);
        alg.insertionSort(numbers);
    }
}

