package com.company;

public class Sort {
  public void bubbleSort(int[] array) {
    int temp;

    System.out.println("This is an implementation of the bubble sort algorithm:");
    for (int output : array)
      System.out.print(output);
    System.out.println("\n");

    for (int pass = array.length-1; pass > 0 ; --pass)
    {
      for (int i = 0; i < pass; ++i) {
        if (array[i] > array[i+1]) {
          temp = array[i];        // swap elements if
          array[i] = array[i+1];  // array[i] > array[i+1]
          array[i+1] = temp;
        }
        for (int output : array)
          System.out.print(output);
        System.out.println();
      }
      System.out.println("--------------------");
    }
  }

  public void selectionSort(int[] array) {
    int temp, smallest, smallestIndex;

    System.out.println("This is an implementation of the selection sort algorithm:");
    for (int output : array)
      System.out.print(output);
    System.out.println("\n");

    for (int i = 0; i < array.length; ++i) {
      smallest = array[i];
      smallestIndex = i;
      for (int j = i+1; j < array.length; ++j) {
        if (array[j] < smallest) {
          smallest = array[j];
          smallestIndex = j;
        }
      }
      if (smallest != array[i]) {
        temp = array[i];    // swap the elements
        array[i] = array[smallestIndex];
        array[smallestIndex] = temp;
      }
      for (int output : array)
        System.out.print(output);
      System.out.println();
    }
  }

  public void insertionSort(int[] array) {
    int temp;

    System.out.println("This is an implementation of the insertion sort algorithm:");
    for (int output : array)
      System.out.print(output);
    System.out.println("\n");

    for (int maxIndex = 1; maxIndex < array.length; ++maxIndex) {
      for (int index = maxIndex; index > 0; --index) {
        if (array[index] < array[index-1]) {
          temp = array[index];
          array[index] = array[index-1];
          array[index-1] = temp;
        }
        for (int output : array)
          System.out.print(output);
        System.out.println();
      }
      System.out.println("-----------");
    }
  }

  public void mergeSort(int[] array) {

  }

}
